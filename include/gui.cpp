#include "./files.cpp"
#include <memory>

void setup_cpp(const std::string &f) {
  // Create the file
  std::unique_ptr<file> a = std::make_unique<file>();

  // Adding the main file
  a->create_folder(f);

  // Setting up source folders
  a->create_folder(f + "/include/");
  a->create_folder(f + "/src/");
  a->create_folder(f + "/impl/");
  a->create_folder(f + "/target/");

  // Creating makefile
  a->create_file(f + "/Makefile");

  // Adding main file
  a->create_file(f + "/src/main.cpp");

  // Adding main.cpp file
  a->add_text_to_file(f + "/src/main.cpp", R"(#include <iostream>

int main(){
  std::cout << "Hello, World!" << std::endl;
})");

  // Adding Makefile
  a->add_text_to_file(f + "/Makefile", R"(
CXX = clang++

TARGET = target/out

SRCS = $(wildcard src/*.cpp impl/*.cpp include/*.cpp include/*.hpp */impl/*.hpp)

CXXFLAGS = -std=c++17 -Wall -Wpedantic

all:
	$(CXX) $(CXXFLAGS) $(SRCS)
	mv a.out target/out
        )");

  // Adding text for the file
  a->add_text_to_file(f + "/include/main.hpp", "#pragma once");

  // Adding git
  a->setup_git(f);
}
