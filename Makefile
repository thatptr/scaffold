CXX = clang++

TARGET = target/out

SRCS = src/main.cpp

CXXFLAGS = -std=c++17 -Wall -Wpedantic

INSTALL = /usr/local/bin/scaffold

all:
	$(CXX) $(CXXFLAGS) $(SRCS) -o $(TARGET)

clean:
	rm $(TARGET)

install:
	mv $(TARGET) $(INSTALL)

