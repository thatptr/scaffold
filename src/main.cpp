#include "scaffold.cpp"

int main(int argc, char **argv) {
  // Check if there if is no input
  if (argv[1] == nullptr) {
    printf("USE: scaffold [project name]\n");
    return 1;
  }

  // If there is something setup project
  setup_cpp(argv[1]);
  return 0;
}
